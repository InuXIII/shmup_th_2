{
    "id": "a0eb9d51-31be-4ce4-950f-58f17dfbaeb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fairy_blue2",
    "eventList": [
        {
            "id": "f59fb1a9-6cbb-4b04-9b2b-96512d4070db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0eb9d51-31be-4ce4-950f-58f17dfbaeb1"
        },
        {
            "id": "1b8027be-b5af-484e-b4c4-5edf052957d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a0eb9d51-31be-4ce4-950f-58f17dfbaeb1"
        },
        {
            "id": "f1fc5088-0178-43a3-906e-1fe78fa84726",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a0eb9d51-31be-4ce4-950f-58f17dfbaeb1"
        },
        {
            "id": "f372cb64-3137-44ae-bf55-56ac74336847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a0eb9d51-31be-4ce4-950f-58f17dfbaeb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d8fe2d19-6d43-47b2-91bb-5658f9fcbe2a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02966b17-e72a-4df4-b7d5-fe9d554ac454",
    "visible": true
}