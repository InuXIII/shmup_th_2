{
    "id": "52281ab6-879e-49ad-8acc-60c630e12bde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_pc",
    "eventList": [
        {
            "id": "f59b835c-3188-4d5e-8fdd-6b8cf02c669c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52281ab6-879e-49ad-8acc-60c630e12bde"
        },
        {
            "id": "a6ec32a1-48a5-4bcd-8a48-b5d1b0e72273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f4b567f-5d6d-4530-af7f-7b20409abb4d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "52281ab6-879e-49ad-8acc-60c630e12bde"
        },
        {
            "id": "fa78538f-e3f7-406f-a8d6-4bfa392fffad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52281ab6-879e-49ad-8acc-60c630e12bde"
        },
        {
            "id": "7dfe4628-58e0-4ab6-89d6-57d10e3c9be8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8fe2d19-6d43-47b2-91bb-5658f9fcbe2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "52281ab6-879e-49ad-8acc-60c630e12bde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8251005-889d-4fe8-943c-406cdee8c83a",
    "visible": true
}