{
    "id": "6f4b567f-5d6d-4530-af7f-7b20409abb4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_satori",
    "eventList": [
        {
            "id": "0585ff26-4f62-457e-8d81-bcc44a6b5e59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f4b567f-5d6d-4530-af7f-7b20409abb4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f14fb222-0cbf-4671-b31c-80bc6b5a782b",
    "visible": true
}