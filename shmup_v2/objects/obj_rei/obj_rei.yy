{
    "id": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rei",
    "eventList": [
        {
            "id": "7f6ec4a6-2b27-49fc-b0ff-5cdb48a9fb91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "3f16c2be-841b-4569-9443-afcc3ac0b9da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "27694a50-079e-43ce-9c7b-30985dab2514",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "825c34e9-50a0-49b7-80d7-116419ec9914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "aa3ceea2-2849-4d04-b964-899923c4565f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 89,
            "eventtype": 5,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "5aef3329-528a-47f9-9e88-6d99cb204f5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "13af2ae1-7219-4587-9f1d-7bdaf2d34628",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "96436547-ff65-467e-9533-c2c1b9cdccc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "04a57bc5-23e0-497b-85e9-a56b64f802ce",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "68e1f8f6-4f63-4388-a1d3-9b6036f4107c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cdaa1464-5be2-450b-9b5d-0d077457e37e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        },
        {
            "id": "b9073576-e5c0-4acc-89f6-a97ba4ae2b17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8fe2d19-6d43-47b2-91bb-5658f9fcbe2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3e5f7e7-e82e-4929-8c16-20d9f66b5f99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5b569e68-ed3f-484d-99e1-930be9e42092",
    "visible": true
}