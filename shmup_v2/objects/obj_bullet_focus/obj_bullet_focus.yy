{
    "id": "fae36dd6-a5c1-4f69-ab57-32ec5e22df85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_focus",
    "eventList": [
        {
            "id": "6ce9d551-b705-4765-9d73-a4f355bc8128",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fae36dd6-a5c1-4f69-ab57-32ec5e22df85"
        },
        {
            "id": "57168a0a-0d5b-4db9-bfc5-e9940f581f80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f4b567f-5d6d-4530-af7f-7b20409abb4d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fae36dd6-a5c1-4f69-ab57-32ec5e22df85"
        },
        {
            "id": "861a78f4-d302-41db-8cce-1c9389051f14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fae36dd6-a5c1-4f69-ab57-32ec5e22df85"
        },
        {
            "id": "147ede70-62bb-4cb8-bddd-fc48f031d640",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8fe2d19-6d43-47b2-91bb-5658f9fcbe2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fae36dd6-a5c1-4f69-ab57-32ec5e22df85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb38f841-f9eb-4a83-b8b4-135d6efb4266",
    "visible": true
}