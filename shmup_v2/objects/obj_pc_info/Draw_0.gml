draw_set_font(fnt_points);
draw_set_colour(c_black);
draw_text(440,520,"Score");
draw_text(440,680,"Lifes");
draw_text(440,740,"Bombs");

draw_text(520,520,global.points);

with(obj_rei)
{
	draw_text(520,680,string(global.plife));
	draw_text(520,740,string(global.bombs));
}
