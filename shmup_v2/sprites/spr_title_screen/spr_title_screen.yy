{
    "id": "c856aed7-0159-485b-997a-1792abc44ce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bbde74c-5793-4877-accb-e49fbc5d402e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c856aed7-0159-485b-997a-1792abc44ce8",
            "compositeImage": {
                "id": "2365cccc-bf9a-4f8c-8fea-88dd26716645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bbde74c-5793-4877-accb-e49fbc5d402e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15eae4ca-5507-4a41-96b5-19754f29059b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bbde74c-5793-4877-accb-e49fbc5d402e",
                    "LayerId": "ceed465c-9a67-429a-85aa-a08aa6e5f7b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ceed465c-9a67-429a-85aa-a08aa6e5f7b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c856aed7-0159-485b-997a-1792abc44ce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}