{
    "id": "bf523c42-7deb-40d2-a905-fcaa9fcfcc19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_boss1",
    "For3D": false,
    "HTile": false,
    "VTile": true,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db194661-5623-4117-9dbf-42b44b3d933d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf523c42-7deb-40d2-a905-fcaa9fcfcc19",
            "compositeImage": {
                "id": "7f968536-a1f2-4143-ad44-75529e131454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db194661-5623-4117-9dbf-42b44b3d933d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2adf8972-cb89-4df0-a7e7-c920465d60bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db194661-5623-4117-9dbf-42b44b3d933d",
                    "LayerId": "c4d4da80-cb11-4326-bc70-56aee2f2170f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c4d4da80-cb11-4326-bc70-56aee2f2170f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf523c42-7deb-40d2-a905-fcaa9fcfcc19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}