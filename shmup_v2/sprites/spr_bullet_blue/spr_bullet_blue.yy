{
    "id": "6c0fdea7-1d4d-49e0-90e6-61938acca224",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86656f97-e2f5-4d0e-8e0a-ad381f9429c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0fdea7-1d4d-49e0-90e6-61938acca224",
            "compositeImage": {
                "id": "ee034924-b8fd-496f-86a4-4bfe49cd3b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86656f97-e2f5-4d0e-8e0a-ad381f9429c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a201d8b-9f4f-4bcc-af7d-abb5998ecd8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86656f97-e2f5-4d0e-8e0a-ad381f9429c1",
                    "LayerId": "140dba67-0756-4a30-b4bc-5411ddd87472"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "140dba67-0756-4a30-b4bc-5411ddd87472",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c0fdea7-1d4d-49e0-90e6-61938acca224",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 6,
    "yorig": 7
}