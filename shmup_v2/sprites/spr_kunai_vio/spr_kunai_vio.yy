{
    "id": "016d06c5-1262-4c52-a594-9e304fb6a428",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_kunai_vio",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 8,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0f9c37b-888a-40c0-ba84-4830cbeb68f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016d06c5-1262-4c52-a594-9e304fb6a428",
            "compositeImage": {
                "id": "3aecee01-2dd2-4d2c-95a2-21a351930be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f9c37b-888a-40c0-ba84-4830cbeb68f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db96431-b32c-4a4f-afc2-7a2b24779052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f9c37b-888a-40c0-ba84-4830cbeb68f8",
                    "LayerId": "9ccf48ed-c6e6-4a2f-8679-3ee638fef0ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "9ccf48ed-c6e6-4a2f-8679-3ee638fef0ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "016d06c5-1262-4c52-a594-9e304fb6a428",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 5,
    "yorig": 9
}