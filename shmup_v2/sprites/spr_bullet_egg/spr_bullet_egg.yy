{
    "id": "c3e418e8-2ff4-4398-9556-428a6512fc4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_egg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 6,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91fbb95b-8154-4f43-8869-5d917003342f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3e418e8-2ff4-4398-9556-428a6512fc4b",
            "compositeImage": {
                "id": "6c799b84-2c50-440a-a99c-d064bb66f96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91fbb95b-8154-4f43-8869-5d917003342f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b032d2-1eb1-417f-ae7c-6d9544cd6ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91fbb95b-8154-4f43-8869-5d917003342f",
                    "LayerId": "6ecc5916-8172-40c0-ab04-088fc61acf62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "6ecc5916-8172-40c0-ab04-088fc61acf62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3e418e8-2ff4-4398-9556-428a6512fc4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 10,
    "yorig": 14
}