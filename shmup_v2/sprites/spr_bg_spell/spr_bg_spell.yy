{
    "id": "ca44112a-e809-4e77-b763-9dc270917af7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_spell",
    "For3D": false,
    "HTile": false,
    "VTile": true,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "002bebaa-74b7-4f09-b400-73a730a3b79e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca44112a-e809-4e77-b763-9dc270917af7",
            "compositeImage": {
                "id": "3bb32dcb-21c0-4ec2-a98a-b99d42a46ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "002bebaa-74b7-4f09-b400-73a730a3b79e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5719d9fb-a365-4522-ad14-e1f92da7b38c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "002bebaa-74b7-4f09-b400-73a730a3b79e",
                    "LayerId": "5e6904ba-c6e3-45b2-8d9e-e1895df20223"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5e6904ba-c6e3-45b2-8d9e-e1895df20223",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca44112a-e809-4e77-b763-9dc270917af7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}