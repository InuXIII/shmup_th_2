{
    "id": "de3fd7bc-fa10-444a-a2b0-68bd95ede702",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ph_sb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7f415af-0d67-401a-8ca6-a4077ddf5213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de3fd7bc-fa10-444a-a2b0-68bd95ede702",
            "compositeImage": {
                "id": "35fd36fb-a22c-4785-bb41-e9f380354780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7f415af-0d67-401a-8ca6-a4077ddf5213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c64d8f-912c-4fb8-a3ec-2a34499fcae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7f415af-0d67-401a-8ca6-a4077ddf5213",
                    "LayerId": "e760e834-a949-4b2c-b403-9b87f154f395"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "e760e834-a949-4b2c-b403-9b87f154f395",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de3fd7bc-fa10-444a-a2b0-68bd95ede702",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}