{
    "id": "927ff5aa-5ba6-4b99-875a-dd1e6d08b6be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_satori_intro_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 27,
    "bbox_right": 255,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bd19575-9266-4dda-8cd4-3f799b8d1156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "927ff5aa-5ba6-4b99-875a-dd1e6d08b6be",
            "compositeImage": {
                "id": "adad35a4-41f5-44a9-8662-548ab3f52b5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd19575-9266-4dda-8cd4-3f799b8d1156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea97289-0722-4267-aca6-e8c8e54d7074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd19575-9266-4dda-8cd4-3f799b8d1156",
                    "LayerId": "3ca2fc36-b64e-43ad-b092-f0360d90bfca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "3ca2fc36-b64e-43ad-b092-f0360d90bfca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927ff5aa-5ba6-4b99-875a-dd1e6d08b6be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}