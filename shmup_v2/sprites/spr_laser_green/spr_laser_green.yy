{
    "id": "2ee87be0-651c-478b-833b-066890667e96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 112,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2767244-86ee-42fb-a49d-36123837f389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ee87be0-651c-478b-833b-066890667e96",
            "compositeImage": {
                "id": "c0a01123-fdbf-4b29-a8d1-9aef1f07236f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2767244-86ee-42fb-a49d-36123837f389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef38a9a-480a-469b-8459-274cd81becec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2767244-86ee-42fb-a49d-36123837f389",
                    "LayerId": "b60526a9-23a9-494c-a75d-29fb61604e98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "b60526a9-23a9-494c-a75d-29fb61604e98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ee87be0-651c-478b-833b-066890667e96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 3,
    "yorig": 6
}