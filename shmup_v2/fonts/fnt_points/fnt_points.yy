{
    "id": "a5795a7e-a05d-4a75-9179-ffaa3db16dc3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_points",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c0609492-644e-4811-865d-a371d5bb016e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2f90b94b-3a50-471c-b32d-e3ecebe7ec4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 55,
                "y": 71
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "493c882d-8828-4eff-b876-afe516c8f208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 45,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a7b9c193-8254-41df-b018-be988ef7ff18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 32,
                "y": 71
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "64d2e6bc-2d7f-4c63-9dc7-7b2c8c1cc3ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 20,
                "y": 71
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "60923328-b2fe-4658-b6b4-03669552794b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ec57e0e5-dd4b-443a-b272-435bd51a4587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 230,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "469e5481-6d1e-4842-b1af-08fb13677faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 224,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "677385dc-9711-41bb-8013-56b876393bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 217,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9dc59314-8b57-4b77-a0ce-384864e1c12b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 209,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "dfdb8d0a-10a8-489c-ba15-d7063b30359e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "74a5b24c-e2ab-4f65-a04b-e0077f0f7145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 196,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ca975643-9cc6-441f-aacd-9b56be84ed64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 179,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5f4301b8-c50b-4e9f-9dd8-153862c1afe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 171,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8dba98d8-7c63-4e73-a226-551ccac70c00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 166,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "20690565-b90a-427f-9f04-decec5c27fce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 157,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f5590999-ed9e-4f27-8087-5deea1b6cdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "92dae8ec-3c37-40f4-b4b9-201ff751cbff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 136,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "13877b62-4e24-4d36-a51f-43a2da936b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 124,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "30478809-c856-4e53-bffe-6302d78a532b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 112,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "56939290-1aa2-4d75-8c09-0bce1339f442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e87fe823-f7ee-484e-8dda-0b420216cad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 184,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "da52da34-a396-40b8-864f-267285bd71c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 70,
                "y": 71
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a15e2dde-7785-4d2e-853d-1c6e61361a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8c786fcf-b64d-49a1-96ef-f0c53dd54d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 94,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "484e287f-b0fb-401b-96a2-d59df90e3dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 117,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3d4f9f65-0806-4583-aea5-ae5073c49399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 111,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "28bad2d9-ab96-4819-9da8-a457b6372a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ef0cde0e-671d-4514-810c-e24913a2d3de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 92,
                "y": 94
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4838a797-dbc0-4d92-b479-055708287e77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 79,
                "y": 94
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "df64735b-1e58-4c6d-a26e-c1087c10003f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 66,
                "y": 94
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "67220fd7-9d88-41c2-9b40-fc9a90538396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 53,
                "y": 94
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0316510a-626c-44d2-a663-b61b9974d27a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 32,
                "y": 94
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f8cb11d2-c350-4b57-8bd3-c4d4892e6e9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 16,
                "y": 94
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3aafee0d-c3ec-4150-8b6e-73aac5962543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "89c40d0b-6db4-4c7e-8bab-ff84ff081f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 237,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "231885aa-433b-40ba-af13-5c022c1c1c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 223,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "26a152da-3ce7-4589-a5ab-163492051f67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f3e2e19c-1975-455b-94ab-7f8a17f2bd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 198,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "20d666ff-c1ef-4801-965d-c97f5dd0761e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 182,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ffd59405-28e2-4b89-8808-2d52ee0abecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 168,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9b95e6a1-0137-4102-83bf-84252e156342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 162,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bfc26d3e-43a5-4fc9-86c3-5d63a840389f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 150,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b1c16d51-f325-4c56-afde-771995f50274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 135,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "56aa78e5-5d44-4c35-8317-c8ac61880c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 122,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0a033b18-99ef-40b6-93e0-01700376d66e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 106,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e2a9e4ae-b0f2-4f7b-9b70-cffa5c03b0fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 85,
                "y": 48
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0df16318-4b34-429c-8e4c-982c8e01e0ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 68,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7ad26b99-736e-4398-be7a-0fe4fe80569b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 55,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "23dcb160-ab71-47e4-b77c-40b6d241378e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 24,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fc8b544e-e601-4c8c-9490-564790706239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "46b0661f-ae24-45ff-b9ef-fe78833aa7a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e13f57af-20f8-45ba-90f7-aae77b855c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d53bb4f2-8fa7-4fb4-a889-374b3c172ce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1445462f-57d2-4be4-9e50-e2d0cd29e27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "eee3df9f-551d-45df-a9f5-17edfc9415e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f910fe8e-aea0-473d-899b-aa9cb81ce68d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a3612cd1-321c-4ac3-be0e-def91e30dfd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "56be290e-ea65-41ce-a97b-42d23e0394ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5d8e559e-84b2-4920-9c9c-45f915e32786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 25
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1b5ef23c-98ca-4fde-bea3-88068a5e752d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "99311ba6-0360-4617-9f0c-cd1bb705c7a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8a8438f6-733b-4f2d-99c7-d1d0e036abc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d3c1b75a-5100-4626-b033-a6a0748b341f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "50e62fa7-fc01-4a69-b449-31925b39761a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9b7c5a68-0fbb-4a25-a305-f73d1cb0e937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e7847777-2171-412c-8a70-e594c249dcfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6742d1b0-fe66-4907-a77f-9735be79bcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "986983fb-f140-4241-8312-97c659e98da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "692308fb-9034-4b96-8675-2d7542a5ffb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f7e8468f-d526-4b76-8ad1-480ae1b936af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3fdcb01f-7c31-4b6b-8d24-f42c7533e51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b72fae18-ed91-4d5c-a88e-4406342b4c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2a3d4df1-e1ef-44e7-80ef-dc9febf5578b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 54,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d403b3d6-b8e2-4cc1-826b-85fe79d19bc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 36,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3a88fb64-b3fe-4843-abdf-0d8706cca886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 24,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d9dc1534-75b1-4195-9218-1a114caaf39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 19,
                "y": 48
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6d4adc68-735c-4c0d-bd6e-e970c74f131a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "10bbdcb8-30a6-4d80-8609-e345f51f8f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "360bd7b6-d423-4b10-b094-b8aece933efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 217,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c419996e-e8e2-4a57-9890-a9935aa6296d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 205,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ea4cb14a-6687-45cb-a4f6-5d903c6fb6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 192,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "57282070-b1c1-4890-a70a-17e30eefa0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 183,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8e64e38a-ce4c-4d4b-b411-578257831256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 43,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "13c4ade4-db6c-4975-bd41-df879eb2d5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 174,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ef3d7e50-c142-4472-9f8b-56f005988366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 150,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b7981002-1d11-4a8e-9e2c-454b191c43b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 137,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d9f2587d-3d81-4215-9c7f-5b1cace1d6f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 120,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "27ae4326-0f68-4303-a2bf-3e71a5f8f8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e4870a3c-3a4a-4cf1-a36b-11fdcca7713e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 94,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b98013c1-34b4-4df8-9358-e549da325c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 82,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "147de5c0-e5a4-42b6-a932-dad94d6dd421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f7e534d1-9462-41ca-94e8-4975d1c29bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 68,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "23cde1e6-e858-4258-a57e-1fa8e16dd77d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 59,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bac9baae-aafa-448b-8038-5ca606d0ffb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 129,
                "y": 94
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "abfcfd6b-096e-47b4-9501-4c0bfe1ef191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 142,
                "y": 94
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6f1cb444-f38a-4f54-adf6-a6319e631020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "7866215d-2086-49cb-982e-0cf8436e8607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "d2bf8d78-f78a-4086-a57f-af5dfb3c5dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "cceaba90-90ba-4c30-92f2-4678bc930773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "a2561b8e-7c94-4080-be9b-7298a09ec6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "fe0ff5c1-c67b-4058-aac2-7a303f635774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "bab8eaf2-30bc-4fb5-afbe-466f21e891ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "da3ea0e2-b439-46c3-9e63-ac9a7a059c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "a8b901a3-7577-42d3-992d-ab7bacc73ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "9b71f002-a1e6-4560-963d-f90539913723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "98323a7e-ca91-4cbd-a9d9-5177249653c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "469ff745-069b-4ace-9fdf-d233f8a91fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "9b35fdf0-3353-4289-b024-1a0ada45587e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "d9d02832-0d14-47fa-bfa0-9d90f31ef608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "dda4a65c-0c1c-4a50-b319-a9064a72a8e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "7fcac34c-ed76-4cde-a9d1-574ef61875c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "2a37c423-804c-4497-aec8-e1a5e54eb1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "5ffe68ac-4853-4844-ae93-a5f7b76d0434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "18acadb9-ca1c-43da-ad60-73fddb57d051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "ff0d3fc5-d00d-4f97-84f2-7578ee20dc2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "8876d183-112a-414a-81ca-fe30d86cef0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "62576d11-72c7-4f27-b19b-a2c3bf08dbe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "363d84f4-5329-41ef-8e4d-02a20a08b873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "cfba2ebd-f12c-4916-b2d8-0b8bca86b78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2d6951c4-147b-40c2-945e-ba218248cd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "1bb3e2e1-ef32-4700-a80f-0a4cc0f38581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "af3fedb6-b94c-4937-8fc1-4c74cd637949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "0c1e871b-2d3f-414f-afcd-bc6f04955558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "7251ad9f-733c-45b4-9659-9fc70a168c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "31075b49-4346-492a-bffd-71d7fa1b6ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "3688cd95-5326-4a78-bcd9-c199e4c40de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "88c1624b-8f4a-496d-930f-d9e47a8947aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "ec0151aa-3323-49e9-95ed-86f19fab2440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "fa927775-0120-45f0-b334-b40bd180c0ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "b152c922-6c4b-4836-9665-be4cad829295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "3780dc87-88ee-4468-b0c7-377a357f3b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "42602f71-c4d0-404c-8e33-74be3b006328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "f17c6540-f897-464e-8d2a-17452de2d43c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "0919526e-e4c9-4f8c-97f2-f79d55dace88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "af316b70-a0ed-40b0-bf4f-9479c3d69276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "0f712950-0a1a-4415-b5d1-ec611ebef4ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "31b42b29-2b40-42b8-b407-a5589e5a1e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "735e4756-02ec-409d-acb2-b34a0d2fae54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "132c7c5e-185a-4faf-9e03-c9c52bf33002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "6f986b45-5f55-486b-90ba-e39cfd4f4fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "d1aa7cf4-f0ea-43b7-a298-294b5a916709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "d2bdc54a-48a4-4004-abba-3e75fa0e9730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "ebe113b3-8cdd-41cf-8348-7fc20fc2b35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "01446bfe-1fea-40df-bd78-bfe9b8716677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "feebd2c4-408c-48da-b2a0-3dcb44f0abf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "c3009ff4-a0b4-4aaa-b1dd-af2229d64b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "dd3efda4-724c-496b-b8dd-ce14e5e50340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "87f5523f-5434-4be6-97bf-0c8a4068af82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "be498515-70fa-486f-8a37-525c3a65b8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "692e08d7-31c9-4b30-9ca4-9f0a2ecf0dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "879948a0-a489-4e68-a5b9-ef0f4ff67123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "50840c86-c081-4a59-ad2d-e130411854d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "a149050d-0af5-430f-9138-39194c5bfdd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "2e72bddf-05bf-437d-802d-4bac878fffd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "0e16c461-7381-4690-8fa6-466284bff183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "2b575b33-890e-4126-a2bd-069f98446126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "f664543d-feea-4f12-b5c1-ec68a907fed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "fc6030ae-3e29-42e5-b5a8-33018703cc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "9f465b4a-8026-4baf-a49e-d6b521fce567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "6d8c4e7c-b743-41aa-9cdf-8fe916828d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "2e40f9fb-f8b6-43f2-9afc-2deafc68832b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "0d7ce518-43c4-4bcd-b850-b93c4ea533e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "5a44d9ae-96f3-457a-b083-fd816195b5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "178d9014-90ce-480b-b5c7-3c23f9401798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "3ba1606e-5aca-47e0-a00b-30afe03652ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "7b6a1f8d-ee96-4b33-8c9b-0289d8d208a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "08010cb0-b713-4c31-ab1e-81e3fa20f780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "839a3695-a0f4-4459-aea7-4fda14116f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "6c13a60b-54d8-4340-b807-a0ec3a29cff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "f84ee8c5-6478-48a4-94dd-a16c75aa38ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "5a62dae2-badf-4ebc-a24f-40b321477390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "2fb39d13-6872-4c57-a0f9-9be004d5142c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "7215413b-ff3a-4e59-9569-b296c76ffe03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "a769e199-6b73-48c4-aa7a-b68ad822ffda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "747baa10-6709-42e4-b620-3a5d915af5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "2c0e4c02-69f7-4c7d-958e-06b6040e25cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "d310a07d-5104-4d7f-b238-9662df9a6885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "4c5bf4bd-02aa-4ed9-bd1c-b96a1c6dcd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "fde7ef75-5245-4710-a6ac-c73356f6bc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "b4a7cc18-6bd6-4e1b-87de-f05a199b0d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "1490c303-1f62-498f-b96a-900af3db57ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "415011b6-76ad-4e6f-b653-d0c0ebaf0120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "ac61c1e0-f72f-4677-94b0-12c28e272f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}