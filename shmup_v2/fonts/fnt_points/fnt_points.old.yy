{
    "id": "a5795a7e-a05d-4a75-9179-ffaa3db16dc3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_points",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cb1bf6cb-3f2d-4d65-807e-7d0ae266ca72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b51084a6-07e5-4ee1-a334-3b278bed1565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 72,
                "y": 134
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9b67a685-e2c4-487c-acc4-b2ca4c6c7308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 59,
                "y": 134
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "13bb72d2-cf64-4984-ba91-d05c961d8262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 134
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e281c663-6bee-4238-bc27-966f8afeba9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 26,
                "y": 134
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2396b2b2-db6a-433a-b3f3-7aed77f6d7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "659b0016-a785-405d-a6ef-f6ad98d3d328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 227,
                "y": 101
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0e1d2892-ddd2-49f1-acf0-eb73734ee869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 220,
                "y": 101
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7fd9b6ac-0da9-47dc-9a0b-fad1d1e37c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 101
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "88bcfe20-589b-4145-8368-f37fe95bc216",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 200,
                "y": 101
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "565a4e16-8c5d-40d3-9e5f-66902255cb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 79,
                "y": 134
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a762a37a-989c-481d-b795-87a10d45421c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 184,
                "y": 101
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e3c38f15-3ff0-4826-b61a-cf52d362c4ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 161,
                "y": 101
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a4d6f4be-c957-48b8-bea7-bda628dcb315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 151,
                "y": 101
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7419790d-9224-4ee1-bb6b-8d2872131125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 144,
                "y": 101
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "696b5885-c05a-48f0-8266-971399950776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 133,
                "y": 101
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c4fbf3f8-2e22-4826-8d09-fa2559e7127a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 118,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8c5481d6-edf2-4b22-b6b3-47af3ffb2c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 107,
                "y": 101
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "de8fe665-0e60-417a-8a8d-b519ff872df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 91,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "eb3ad23f-f1fb-450c-b086-464d6f1272ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 76,
                "y": 101
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "62925716-a3cd-404d-8177-47b8b9c9595c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 59,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "861004e0-7907-427b-9680-464565413fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 168,
                "y": 101
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d7c67345-156d-4413-8d41-b8479d24ceb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 91,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "37fcbedb-929b-40a9-996b-f2a638411f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 134
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a1d46371-23d4-4187-b02f-f3e4039c08e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c49d4662-bd73-4f92-8f63-755b1c78a20b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 200
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ece3750b-14d8-4e67-a82b-bc093ab860a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 241,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1f331ba3-d323-4560-886b-c1a2ad31a789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 234,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "24c1d3d8-b7eb-435b-9a37-041ae91d667e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 218,
                "y": 167
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "38a27e37-1432-4b5f-8b21-42256381f554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 202,
                "y": 167
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fbc9e4e2-0244-44dc-b7c8-9edf75d07e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 186,
                "y": 167
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "09f94ea5-3ff3-4c47-b0b6-3e19f0f82b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 169,
                "y": 167
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fb05fc3a-4ad6-4916-ba5b-8f38899f568d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 140,
                "y": 167
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "085fa48c-4f8c-4bbe-b51e-275725dcc084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 118,
                "y": 167
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5635b016-2c70-402f-9529-0ceffcb3d606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 98,
                "y": 167
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "00637341-8f3d-49d2-8dbc-2cad14731d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 78,
                "y": 167
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a78b51df-abd4-4abf-8fd4-fad81be7e45f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 58,
                "y": 167
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8428b88e-7a83-4b40-b5e8-c8a961065180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 40,
                "y": 167
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "aba337a4-941e-426f-936b-358594589ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 23,
                "y": 167
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "78d30a54-6f60-4dcb-9191-ce057fd4c693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b6f14fb1-b21a-4936-9fcb-aac479479528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 217,
                "y": 134
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "28c70995-f277-4a85-bdc6-1239e98dd6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 210,
                "y": 134
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "17e2103e-46d0-42a1-9482-e4c216a44793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 195,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1801ae00-8bb0-4e72-b5f8-ffbe9cf2d476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 175,
                "y": 134
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "75c8dfd2-de84-4630-be89-03789ab16e78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 159,
                "y": 134
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8dbb1207-70af-40ad-bdc4-bce35ee25fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 137,
                "y": 134
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d3d6ca36-7385-4d51-9730-1c7b92877372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 41,
                "y": 101
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a18e4cde-ff73-4c5e-bcdf-6d2fe57be036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 20,
                "y": 101
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bced61c4-52f8-4ff9-b055-d391eeaea6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "929673a0-0b24-496c-8141-e03e1ff19a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 138,
                "y": 35
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "23add70a-0f04-420c-b8e6-a44d9610d2bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 107,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6b31fa97-3502-4c71-8f1b-bc35e80cf6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 88,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "37c7e786-6ec3-4187-a583-98b5f9859962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 70,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "43fe9727-679e-4413-a530-25b9ec5ebcff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bc371590-445e-4bf3-b359-9143b1eac499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 30,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f317cfa4-d375-4293-9b5d-efc4982dd632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5d27c4ee-cf31-4440-8e7f-e8d405e224c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0d950a02-5471-4ef6-bcac-a54da66a5a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0012d0f0-24d3-4e8a-b411-6cfab6b6900e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "697bdc71-1d6b-45bf-b06b-b7b66c2ff1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 128,
                "y": 35
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b868a4af-8d36-4eaf-8f0b-c968ae984bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6f2cda16-8cca-4db2-bb71-9a18fedb1632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b50ef7c6-71c3-4c32-b19a-6a0c0f93ba43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "97103df7-03c1-4665-bd2f-55ea676a34e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "52852594-8db6-4560-a5b4-e2cc71ef613b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d74af147-5eba-4610-9aa0-dc77a3c58c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "04b64582-70b8-4a6d-aa79-3e97c2409c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "27e1f0df-44cf-4d39-8bbc-940c0f750182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0218d33d-80de-4b45-8183-bcd9ba971671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a9c24b21-6509-44ce-bef7-a6fbbdb790ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ac75b3b6-3b21-46ac-b6eb-ec66cb0f0c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a80fe4a6-412b-47be-af26-88c44c935dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 160,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d8b221ed-8e03-474f-b33c-d176568adcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 75,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1323558a-bb4e-45dd-8a66-29df43f57fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 176,
                "y": 35
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "578d56bf-ec57-41c6-ab7a-ed2af384e11a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": -2,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d725e9f5-ee47-4442-89da-fc58e406e925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 211,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d3ff1d3e-5962-402c-b735-13a1c2441c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 204,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "eb3df3fb-4a13-4de2-bb02-a949b0a46a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 180,
                "y": 68
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7ff1d39a-483e-4ff5-8ec2-b0b44522c2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 164,
                "y": 68
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8b3f7299-1503-44d1-bb3b-bebe8d2296c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 147,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "779473e9-1009-4131-bd59-2fd095c48fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1294369b-4833-469d-9515-a41905276365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 114,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ba3ec1d4-5d2f-4183-ba65-f23f4f9eb831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 102,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "14b05bae-0013-4387-aad6-21979d15c58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 237,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "97f39dcc-1dd6-4b66-aaf5-2df52849d61c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a2d26af5-bc2d-4b3b-8021-7420674505cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 59,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b59e0af8-c591-4c84-92ff-2e0e39646e14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c463271e-6cd9-4544-a5e4-95289ae34058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 19,
                "y": 68
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "50e280be-25f9-4891-a831-a1b261110dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "262204c6-6de1-4ebe-9e45-58142b158c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 228,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cd772678-dc71-4d11-b132-f3929b6e36be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 213,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d5a9e27a-3ba8-4ed3-a86e-07c3c23452ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1dc99a28-4d16-4b11-bcff-88c34919776b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 195,
                "y": 35
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6937a02d-e88a-415f-a64c-b96be689e78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 183,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "16e1418f-a286-4f99-b0f2-ac3ab06ea673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 18,
                "y": 200
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "245a5a80-2610-4d61-b3ce-5dff0b4dba58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 31,
                "offset": 5,
                "shift": 26,
                "w": 16,
                "x": 35,
                "y": 200
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "41c33a09-7662-41f5-bbdc-a9a53a755484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "7f76cfa5-9d1c-43c9-abb8-975a5533f5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "b66f09bd-19c1-4724-8192-4c66291f6aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "f80a8652-56c5-463d-84f2-878fa9695552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "cbc680bd-e621-4b62-b132-1a92e482ed62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "17c4d9ac-49da-4733-8f07-31fe3b53f7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "502070f2-a278-46b9-9b39-e47341f1b4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "34b43769-a0cb-49c4-a120-2897a789d8f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "e312a125-4035-45c3-899e-442b3ef977e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "14aae059-8cf5-4382-b3b2-6ffa8ec481a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "f7fb6d8b-2711-4b34-858d-d8d8d64a1e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "14a896b1-fe4c-4faf-9fc6-b8a50f390e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "f4cd6b59-e379-4837-9045-a135a38ad704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "e99337ce-c32b-4024-891a-0f231ce78a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "a3630759-9a2a-4c97-bcdc-75bae589f2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "63981438-a338-4d26-9880-6be31653c369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "1c6af72c-7a5e-4146-82e0-6c5cfcc4e966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "df069738-ceed-401e-bff7-fae455a02e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e2ddfd3f-0834-4bf6-9c19-6185f8b495c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "8b2998a7-e533-4256-916e-b539901cccb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "7dfddcbe-5829-4c39-a5fb-10ec14a6a248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "743df55a-37ce-441b-8f59-b0962c9dcf73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "f58142d1-fe32-4096-b935-0a0503d95276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "5fb5ed54-4c84-470a-947a-42f215b2d845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "9da7ef8e-7715-4d7b-92f6-b6ab67766801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "41f60688-4b07-4fec-847a-2728ebb5eb01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "a771aa6d-58dd-4088-a98f-a368e4c64a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "80d1be34-be3f-4c29-8886-d39980811eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "1be369fe-8ce5-4fd4-8638-6c7e61b2ed54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "9cf4e1ce-4b7a-4fa9-acd4-54a5e68e6a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "d2693b12-bd4a-4d68-b25c-1f9655bde681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "c1639023-6543-45cd-a692-f46631ca4a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "db7aeb57-1c7a-4c1a-a676-4126f1ec80ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "be8065d7-4c15-4f88-9f94-98be9e4fa751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "917c0d43-05cc-418f-a270-1c72f473f8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "aea35a15-9936-4a8a-915d-13e98db69392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "c9475d16-f183-49af-9443-72b004bcd0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "08a904e9-404b-4809-bedf-5fb858eed9f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "1cbdb646-79ab-4b59-836e-1ed9c1368fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "76a0459f-898a-4382-bc7b-dfcea4220480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "cb1e54fb-8b6a-4ccf-91d9-b8995e3dc997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "c25d047b-250e-4b34-aac6-1df0e6b21cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "46f4218f-a8ca-4106-aa7f-11279e0b913d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "7b6d6478-75f5-49f0-bded-85dfc116503e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "7efda698-eeff-473e-9ad2-77e4c7d25c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "316e803f-850c-4532-ae2b-a4562881b48a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "55410a23-0f49-4993-a63f-2ec04915455e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "ca4fe08e-3aa2-4ab0-9204-7661fdbc0d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "402d2d29-3eda-4d57-b46f-9e8c5447f0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "c81eb794-75ac-40f1-9035-a119f7bdaac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "5c20d7b3-176d-43e3-9203-22ebbd4724fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "77ce39ba-37cc-4e26-8c56-a582caacdb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "346b768b-1d34-4ddf-bdfb-ace350f56de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "76a60b8a-47fa-4cee-9aee-abe4b08ad67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "528e7659-ceac-440b-9a3d-930a30b55b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "74c3ed42-7b65-409c-9022-33d488c9c9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "233ee73b-8ecd-4565-b7aa-ba29aac3b81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "6bfede3f-f863-4196-8204-b71ed056c647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "42ced6fb-379d-4cbf-8acf-159564e742a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "5d7e6595-7a9b-449f-978b-5e42d21431cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "2255f0a5-9307-4848-873b-75d96e7cf0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "168938ed-9e30-46b5-ad56-6dcabda345a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "a0b9b11d-dbc0-460e-83c7-34fd665bf8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "4c226844-6320-4bcf-80cc-6bbc5b3f0de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "4ea554dd-020c-4c42-9065-02d898016a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "42d3b745-9b51-4965-81af-fb5c7b9772a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "d75be281-bd00-4f2e-a67f-a9201bf27568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "c7da9de3-c07e-41e8-8297-f233b1072e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "2f9a4159-a744-4bae-8b23-f400619f46a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "225fad37-dbea-43d1-8969-b3cf9cd4ad97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "5f4611b0-7ed8-4a2d-81a8-0e75dc48baaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "8b39fb6e-ae69-43d9-94b7-52910b3d2cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "4214ba2b-6a60-44c9-894a-dfd32cded42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "296e45c1-ca23-4b84-ba88-b635628212f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "5ced5074-91f8-42b8-a804-f6072b7a0234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "77e8f313-ef8a-4491-a695-fbb6b7546816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "8dfb0c4d-db36-4fa6-a336-004c24b7015c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "c4d23213-3ecb-4c21-9d03-912b63bb1bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "9674ed1c-04c1-42c9-8471-1ea61e8673fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "111524ca-2566-4cb8-9640-ecbac4bd28ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "81a13a2c-0736-4977-b061-3d55ad35916e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "a7d3975b-3069-45e4-823f-afef5d8702fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "f4d3377c-e36b-4bc1-80dd-1775aa25a701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "4e9bbdde-12bd-4633-badf-a9525f67bbc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "eb8c5475-8dd5-42a5-a45e-909cc1de4ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "c6bb8e72-580a-4882-957b-1eebaa271d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "c33a38cc-dee0-4d78-a87a-6719bcd76071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "10d52693-fcb7-4662-82e6-de47899d997b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "e280ca95-6858-4c85-b84c-ae47bc0c6645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "0b3a453e-7932-4083-b853-69c4d0b4c940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 20,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}